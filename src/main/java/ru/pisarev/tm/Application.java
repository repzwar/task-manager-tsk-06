package ru.pisarev.tm;

import ru.pisarev.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (runArgs(args)) exit();
        process();
    }

    private static boolean runArgs (final String[] args){
        if (args == null || args.length == 0)  return false;
        run(args[0]);
        return true;
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
            default:
                displayError();
        }
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.CMD_VERSION + " - Display program version.");
        System.out.println(TerminalConst.CMD_ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.CMD_HELP + " - Display list of terminal commands.");
        System.out.println(TerminalConst.CMD_EXIT + " - Close application.");
    }

    private static void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

    private static void displayWait() {
        System.out.println();
        System.out.println("Enter command.");
    }

    private static void displayVersion() {
        System.out.println("0.6.0");
    }

    private static void displayAbout() {
        System.out.println("Aleksey Pisarev");
        System.out.println("pisarevaleks@mail.ru");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            displayWait();
            command = scanner.nextLine();
            run(command);
        }
    }

}
